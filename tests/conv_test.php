<?php

function convert($entry) {
	return round((((($entry)-32)*5)/9),2);
}

// Include test library
require_once 'simpletest/unit_tester.php';
require_once 'simpletest/reporter.php';
require_once '../classes/convlist.php';

class TestConversion extends UnitTestCase{
	
  function TestConvert(){
  	$conv = new ConvList();
  	$convlist = $conv->returnAll();

  	$countgtzero = (count($convlist)>0);
  	$this->assertTrue($countgtzero);
  	if($countgtzero) {
  		$this->assertEqual(100, convert($convlist[0]));
  		$this->assertEqual(50, convert($convlist[1]));
  		$this->assertEqual(200, convert($convlist[2]));
  	}
    //$this->assertEqual(212, c_to_f(100));
  }
	
}

// run test
$test = new TestConversion( 'Testing F to C Conversion' );
$test->run(new HTMLReporter());

?>